#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Agent.o \
	${OBJECTDIR}/GraphPane.o \
	${OBJECTDIR}/MainApp.o \
	${OBJECTDIR}/MainFrame.o \
	${OBJECTDIR}/MainThread.o \
	${OBJECTDIR}/Parameters.o \
	${OBJECTDIR}/Population.o \
	${OBJECTDIR}/RandomSimple.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=`wx-config --cxxflags` 
CXXFLAGS=`wx-config --cxxflags` 

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=`wx-config --libs`  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/wxfunctionalresponse

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/wxfunctionalresponse: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/wxfunctionalresponse ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Agent.o: Agent.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DWX_PRECOMP -I/usr/local/include/wx-3.1 -I/usr/local/lib/wx/include/gtk3-unicode-3.1 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Agent.o Agent.cpp

${OBJECTDIR}/GraphPane.o: GraphPane.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DWX_PRECOMP -I/usr/local/include/wx-3.1 -I/usr/local/lib/wx/include/gtk3-unicode-3.1 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GraphPane.o GraphPane.cpp

${OBJECTDIR}/MainApp.o: MainApp.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DWX_PRECOMP -I/usr/local/include/wx-3.1 -I/usr/local/lib/wx/include/gtk3-unicode-3.1 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MainApp.o MainApp.cpp

${OBJECTDIR}/MainFrame.o: MainFrame.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DWX_PRECOMP -I/usr/local/include/wx-3.1 -I/usr/local/lib/wx/include/gtk3-unicode-3.1 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MainFrame.o MainFrame.cpp

${OBJECTDIR}/MainThread.o: MainThread.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DWX_PRECOMP -I/usr/local/include/wx-3.1 -I/usr/local/lib/wx/include/gtk3-unicode-3.1 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MainThread.o MainThread.cpp

${OBJECTDIR}/Parameters.o: Parameters.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DWX_PRECOMP -I/usr/local/include/wx-3.1 -I/usr/local/lib/wx/include/gtk3-unicode-3.1 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Parameters.o Parameters.cpp

${OBJECTDIR}/Population.o: Population.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DWX_PRECOMP -I/usr/local/include/wx-3.1 -I/usr/local/lib/wx/include/gtk3-unicode-3.1 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Population.o Population.cpp

${OBJECTDIR}/RandomSimple.o: RandomSimple.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -DWX_PRECOMP -I/usr/local/include/wx-3.1 -I/usr/local/lib/wx/include/gtk3-unicode-3.1 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RandomSimple.o RandomSimple.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
