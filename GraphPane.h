/* 
 * File:   GraphPane.h
 * Author: philju
 *
 * Created on 02 April 2020, 15:43
 */

#ifndef GRAPHPANE_H
#define GRAPHPANE_H

class MainThread;

#include "Population.h"
//#include "MainThread.h"

#include "wx/wx.h"
#include "wx/sizer.h"

class GraphPane : public wxPanel {
public:
    GraphPane( wxFrame*, Population* );
    ~GraphPane( );

    void StopThread( );
    void Redraw( );

    void OnPaint( wxPaintEvent& );
    void OnEraseBackground( wxPaintEvent& );
    void OnMouse( wxMouseEvent& );

    DECLARE_EVENT_TABLE( )

private:
    void Draw( wxDC& );
    void PaintBackground( wxDC& );

    MainThread* mThread;
    Population* mPopulation;
    bool mThreadingIsRunning;
};

#endif
