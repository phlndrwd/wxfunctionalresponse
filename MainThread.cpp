#include "MainThread.h"
#include "Parameters.h"

#include <iostream>

MainThread::MainThread( Population* population, GraphPane* graphPane, bool* threadIsRunning ) {
    mPopulation = population;
    mGraphPane = graphPane;
    mThreadIsRunning = threadIsRunning;
}

void* MainThread::Entry( ) {
    for( unsigned i = 0; i < Parameters::GetNumberOfIterations( ); ++i ) {
        for( unsigned t = 0; t < Parameters::GetNumberOfTimeSteps( ); ++t ) {
            if( *mThreadIsRunning == true ) {
                mPopulation->Update( );
                mGraphPane->Redraw( );
                std::cout << "i=" << i << ", t=" << t << std::endl;
            }
        }
        std::cout << "Encounters> " << mPopulation->GetNumberOfEncounters( ) << std::endl;
        mPopulation->Reset( );
    }
    std::cout << "Average> " << mPopulation->GetAverage( ) << std::endl;
    *mThreadIsRunning = false;
}

void MainThread::Stop( ) {
    *mThreadIsRunning = false;
}