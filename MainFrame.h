#ifndef MAINFRAME_H
#define MAINFRAME_H

#include <wx/wx.h>

class MainFrame : public wxFrame {
public:
    MainFrame( const wxString& );
    ~MainFrame( );

    void OnPaint( wxPaintEvent & event );
};

enum {
    ID_ResFR = 1
};

#endif

