/*
 * File:   Population.h
 * Author: philju
 *
 * Created on 01 April 2020, 17:57
 */

#ifndef POPULATION_H
#define POPULATION_H

#include "Agent.h"

#include "wx/wx.h"

#include <vector>

class Population {
public:
    Population( );
    ~Population( );

    void Update( );
    void Reset( );
    double GetAverage( );

    unsigned GetNumberOfAgents( );
    unsigned GetNumberOfEncounters( );
    std::vector< unsigned > GetEncounters( );

    Agent* GetAgent( unsigned );
    std::vector<Agent*> GetAgents( );

private:
    void Populate( );
    void Movement( );
    void Collision( );

    unsigned mNumberOfEncounters;
    Agent* mPredator;
    std::vector< unsigned > mEncounters;
    std::vector< Agent* > mAgents;
};

#endif /* POPULATION_H */

