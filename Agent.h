#ifndef AGENT_H
#define AGENT_H

class Agent {
public:
    Agent( double, double, double );

    double GetX( );
    double GetY( );
    double GetDiameter( );
    double GetRadius( );
    double GetArea( );
    double GetVolume( );
    double GetXSpeed( );
    double GetYSpeed( );
    unsigned GetInactiveCount( );
    bool IsPredator( );

    void SetX( double );
    void SetY( double );
    void SetDiameter( double );
    void SetXSpeed( double );
    void SetYSpeed( double );
    void SetInactiveCount( unsigned );
    void SetPredator( bool );

private:
    double mX;
    double mY;
    double mDiameter;

    double mRadius;
    double mArea;
    double mVolume;
    double mXSpeed;
    double mYSpeed;

    unsigned mInactiveCount;
    bool mIsPredator;
};

#endif /* AGENT_H */

