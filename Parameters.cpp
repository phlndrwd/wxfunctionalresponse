#include "Parameters.h"

unsigned Parameters::mPopulationSize = 500;
unsigned Parameters::mNumberOfTimeSteps = 100;
unsigned Parameters::mNumberOfIterations = 100;
unsigned Parameters::mHandlingTime = 10;
unsigned Parameters::mRandomSeed = 0;

Parameters::Parameters( ) {
}

unsigned Parameters::GetPopulationSize( ) {
    return mPopulationSize;
}

unsigned Parameters::GetNumberOfTimeSteps( ) {
    return mNumberOfTimeSteps;
}

unsigned Parameters::GetNumberOfIterations( ) {
    return mNumberOfIterations;
}

unsigned Parameters::GetHandlingTime( ) {
    return mHandlingTime;
}

unsigned Parameters::GetRandomSeed( ) {
    return mRandomSeed;
}

void Parameters::SetPopulationSize( unsigned populationSize ) {
    mPopulationSize = populationSize;
}

void Parameters::SetNumberOfTimeSteps( unsigned numberOfTimeSteps ) {
    mNumberOfTimeSteps = numberOfTimeSteps;
}

void Parameters::SetNumberOfIterations( unsigned numberOfIterations ) {
    mNumberOfIterations = numberOfIterations;
}

void Parameters::SetHandlingTime( unsigned handlingTime ) {
    mHandlingTime = handlingTime;
}

void Parameters::SetRandomSeed( unsigned randomSeed ) {
    mRandomSeed = randomSeed;
}