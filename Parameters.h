#ifndef PARAMETERS_H
#define PARAMETERS_H

class Parameters {
public:
    static unsigned GetPopulationSize( );
    static unsigned GetNumberOfTimeSteps( );
    static unsigned GetNumberOfIterations( );
    static unsigned GetHandlingTime( );
    static unsigned GetRandomSeed( );

    static void SetPopulationSize( unsigned );
    static void SetNumberOfTimeSteps( unsigned );
    static void SetNumberOfIterations( unsigned );
    static void SetHandlingTime( unsigned );
    static void SetRandomSeed( unsigned );

private:
    static unsigned mPopulationSize;
    static unsigned mNumberOfTimeSteps;
    static unsigned mNumberOfIterations;
    static unsigned mHandlingTime;
    static unsigned mRandomSeed;
    Parameters( );
};

#endif /* PARAMETERS_H */

