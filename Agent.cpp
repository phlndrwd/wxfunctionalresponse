#include "Agent.h"
#include "RandomSimple.h"
#include "Parameters.h"

#include <math.h>

Agent::Agent( double x, double y, double diameter ) {
    mX = x;
    mY = y;
    mDiameter = diameter;

    mIsPredator = false;

    mRadius = diameter / 2;
    mArea = M_PI * std::pow( mRadius, 2 );
    mVolume = ( 4 / 3 ) * mArea * mRadius;

    double randomAngle = RandomSimple::Get( )->GetUniform( ) * 360;

    mXSpeed = std::cos( ( randomAngle * M_PI ) / 180 );
    mYSpeed = std::sin( ( randomAngle * M_PI ) / 180 );
    
    mInactiveCount = 0;
}

double Agent::GetX( ) {
    return mX;
}

double Agent::GetY( ) {
    return mY;
}

double Agent::GetDiameter( ) {
    return mDiameter;
}

double Agent::GetRadius( ) {
    return mRadius;
}

double Agent::GetArea( ) {
    return mArea;
}

double Agent::GetVolume( ) {
    return mVolume;
}

double Agent::GetXSpeed( ) {
    return mXSpeed;
}

double Agent::GetYSpeed( ) {
    return mYSpeed;
}

unsigned Agent::GetInactiveCount( ) {
    return mInactiveCount;
}

bool Agent::IsPredator( ) {
    return mIsPredator;
}

void Agent::SetX( double x ) {
    mX = x;
}

void Agent::SetY( double y ) {
    mY = y;
}

void Agent::SetDiameter( double diameter ) {
    mDiameter = diameter;
}

void Agent::SetXSpeed( double xSpeed ) {
    mXSpeed = xSpeed;
}

void Agent::SetYSpeed( double ySpeed ) {
    mYSpeed = ySpeed;
}

void Agent::SetInactiveCount( unsigned inactiveCount ) {
    mInactiveCount = inactiveCount;
}

void Agent::SetPredator( bool predator ) {
    mIsPredator = predator;
}
