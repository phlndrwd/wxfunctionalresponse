/*
 * File:   main.h
 * Author: philju
 *
 * Created on 30 March 2020, 20:56
 */

#ifndef MAINAPP_H
#define MAINAPP_H

#include "MainFrame.h"
#include "GraphPane.h"
#include "Population.h"

#include <wx/wx.h>

class MainApp : public wxApp {
public:
    virtual bool OnInit( );
    virtual int OnExit( );

private:
    MainFrame* mMainFrame;
    GraphPane* mGraphPane;
    Population* mPopulation;
};

#endif

