#include "Population.h"

#include "Parameters.h"
#include "Constants.h"
#include "RandomSimple.h"

Population::Population( ) {
    mNumberOfEncounters = 0;
    Populate( );
}

Population::~Population( ) {
    for( unsigned i = 0; i < Parameters::GetPopulationSize( ); ++i ) {
        delete mAgents[i];
    }
    mEncounters.clear( );
}

void Population::Update( ) {
    Movement( );
    Collision( );
    unsigned count = mPredator->GetInactiveCount( );
    if( count != 0 ) {
        count = count - 1;
        mPredator->SetInactiveCount( count );
    }
}

void Population::Reset( ) {
    mEncounters.push_back( mNumberOfEncounters );
    mPredator->SetInactiveCount( 0 );
    mNumberOfEncounters = 0;
}

double Population::GetAverage( ) {
    double sum = 0;
    for( unsigned i = 0; i < mEncounters.size( ); ++i ) {
        sum += mEncounters[ i ];
    }
    double average = sum / ( double ) mEncounters.size( );
    return average;
}

void Population::Populate( ) {
    mAgents.clear( );
    for( int i = 0; i < Parameters::GetPopulationSize( ); ++i ) {
        double randomX = RandomSimple::Get( )->GetUniform( ) * Constants::cFrameWidth;
        double randomY = RandomSimple::Get( )->GetUniform( ) * Constants::cFrameWidth;
        //double randomDiameter = Parameters.getMinimumDiameter() + (Parameters.getRandom().nextDouble() * (Parameters.getMaximumDiameter() - Parameters.getMinimumDiameter()));
        double randomDiameter = 10;

        mAgents.push_back( new Agent( randomX, randomY, randomDiameter ) );
    }
    unsigned randIndex = RandomSimple::Get( )->GetUniformInt( mAgents.size( ) - 1 );
    mPredator = mAgents[ randIndex ];
    mPredator->SetPredator( true );
}

void Population::Movement( ) {
    for( unsigned i = 0; i < mAgents.size( ); ++i ) {
        Agent* agent = mAgents[i];

        if( agent->GetInactiveCount( ) == 0 ) {
            double newX = agent->GetX( ) + agent->GetXSpeed( );
            double newY = agent->GetY( ) + agent->GetYSpeed( );

            // Apply toroidal geometry
            if( newX < 0 ) {
                newX = Constants::cFrameWidth + newX;
            } else if( newX > Constants::cFrameWidth ) {
                newX = newX - Constants::cFrameWidth;
            }

            if( newY < 0 ) {
                newY = Constants::cFrameHeight + newY;
            } else if( newY > Constants::cFrameHeight ) {
                newY = newY - Constants::cFrameHeight;
            }

            agent->SetX( newX );
            agent->SetY( newY );
        }
    }
}

void Population::Collision( ) {
    unsigned numberOfAgents = mAgents.size( );
    for( unsigned i = 0; i < numberOfAgents; ++i ) {

        Agent* firstAgent = mAgents[i];

        if( firstAgent->GetInactiveCount( ) == 0 ) {

            for( unsigned j = 0; j < numberOfAgents; ++j ) {

                Agent* secondAgent = mAgents[j];

                if( secondAgent->GetInactiveCount( ) == 0 ) {

                    // TODO: Does not take account for agents on the edge of the grid.
                    if( firstAgent != secondAgent ) {
                        double firstX = firstAgent->GetX( );
                        double firstY = firstAgent->GetY( );
                        double secondX = secondAgent->GetX( );
                        double secondY = secondAgent->GetY( );

                        double lengthX = firstX - secondX;
                        double lengthY = firstY - secondY;
                        double distance = std::sqrt( ( lengthX * lengthX ) + ( lengthY * lengthY ) );

                        distance = distance - firstAgent->GetRadius( ) - secondAgent->GetRadius( );
                        if( distance <= 0 ) {
                            double firstNewXSpeed = ( firstAgent->GetXSpeed( ) * ( firstAgent->GetVolume( ) - secondAgent->GetVolume( ) ) + ( 2 * secondAgent->GetVolume( ) * secondAgent->GetXSpeed( ) ) ) / ( firstAgent->GetVolume( ) + secondAgent->GetVolume( ) );
                            double firstNewYSpeed = ( firstAgent->GetYSpeed( ) * ( firstAgent->GetVolume( ) - secondAgent->GetVolume( ) ) + ( 2 * secondAgent->GetVolume( ) * secondAgent->GetYSpeed( ) ) ) / ( firstAgent->GetVolume( ) + secondAgent->GetVolume( ) );
                            double secondNewXSpeed = ( secondAgent->GetXSpeed( ) * ( secondAgent->GetVolume( ) - firstAgent->GetVolume( ) ) + ( 2 * firstAgent->GetVolume( ) * firstAgent->GetXSpeed( ) ) ) / ( firstAgent->GetVolume( ) + secondAgent->GetVolume( ) );
                            double secondNewYSpeed = ( secondAgent->GetYSpeed( ) * ( secondAgent->GetVolume( ) - firstAgent->GetVolume( ) ) + ( 2 * firstAgent->GetVolume( ) * firstAgent->GetYSpeed( ) ) ) / ( firstAgent->GetVolume( ) + secondAgent->GetVolume( ) );

                            firstAgent->SetXSpeed( firstNewXSpeed );
                            firstAgent->SetYSpeed( firstNewYSpeed );
                            secondAgent->SetXSpeed( secondNewXSpeed );
                            secondAgent->SetYSpeed( secondNewYSpeed );

                            firstAgent->SetX( firstAgent->GetX( ) + firstNewXSpeed );
                            firstAgent->SetY( firstAgent->GetY( ) + firstNewYSpeed );
                            secondAgent->SetX( secondAgent->GetX( ) + secondNewXSpeed );
                            secondAgent->SetY( secondAgent->GetY( ) + secondNewYSpeed );


                            Agent* predator = NULL;
                            if( firstAgent->IsPredator( ) == true ) predator = firstAgent;
                            if( secondAgent->IsPredator( ) == true ) predator = secondAgent;

                            if( predator != NULL ) {
                                predator->SetInactiveCount( Parameters::GetHandlingTime( ) );
                                ++mNumberOfEncounters;
                            }
                        }
                    }
                }
            }
        }
    }
}

unsigned Population::GetNumberOfAgents( ) {
    return mAgents.size( );
}

unsigned Population::GetNumberOfEncounters( ) {
    return mNumberOfEncounters;
}

std::vector< unsigned > Population::GetEncounters( ) {
    return mEncounters;
}

Agent* Population::GetAgent( unsigned i ) {
    return mAgents[i];
}

std::vector<Agent*> Population::GetAgents( ) {
    return mAgents;
}
