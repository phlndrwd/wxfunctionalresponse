#include "GraphPane.h"
#include "Constants.h"
#include "Population.h"
#include "MainThread.h"

#include <wx/dcbuffer.h>
#include <iostream>

GraphPane::GraphPane( wxFrame* parent, Population* population ):
wxPanel( parent, wxID_ANY, wxDefaultPosition, wxSize( Constants::cFrameHeight, Constants::cFrameWidth ) ) {
    mPopulation = population;
    mThreadingIsRunning = false;
}

GraphPane::~GraphPane( ) {

}

void GraphPane::StopThread( ) {
    mThread->Stop( );
}

void GraphPane::Redraw( ) {
    wxWindow::Refresh( );
    //wxWindow::Update();
}

/*
 * Called by the system of by wxWidgets when the panel needs
 * to be redrawn. You can also trigger this call by
 * calling Refresh()/Update().
 */

void GraphPane::OnPaint( wxPaintEvent& evt ) {
    wxBufferedPaintDC dc( this );
    Draw( dc );
}

void GraphPane::OnEraseBackground( wxPaintEvent& evt ) {
    // Empty implementation to prevent flicker
}

void GraphPane::OnMouse( wxMouseEvent& evt ) {
    if( mThreadingIsRunning == false ) {
        mThreadingIsRunning = true;
        mThread = new MainThread( mPopulation, this, &mThreadingIsRunning );
        if( mThread->Create( ) != wxTHREAD_NO_ERROR ) {
            std::cout << "ERROR> Cannot create thread..." << std::endl;
            mThread->TestDestroy( );
            mThread->Delete( );
        } else {
            mThread->Run( );
        }
    } else {
        std::cout << "ERROR> Thread is already running!" << std::endl;
    }
}

/*
 * Here we do the actual rendering. I put it in a separate
 * method so that it can work no matter what type of DC
 * (e.g. wxPaintDC or wxClientDC) is used.
 */
void GraphPane::Draw( wxDC& dc ) {
    PaintBackground( dc );
    for( int i = 0; i < mPopulation->GetNumberOfAgents( ); ++i ) {
        Agent* agent = mPopulation->GetAgent( i );
        if( agent->IsPredator( ) == true ) {
            dc.SetPen( *wxRED_PEN );
            dc.SetBrush( *wxRED_BRUSH );
        } else {
            dc.SetPen( *wxWHITE_PEN );
            dc.SetBrush( *wxWHITE_BRUSH );
        }
        dc.DrawCircle( agent->GetX( ), agent->GetY( ), agent->GetRadius( ) );
    }
}

void GraphPane::PaintBackground( wxDC& dc ) {
    dc.SetBrush( *wxBLACK_BRUSH );
    dc.DrawRectangle( 0, 0, Constants::cFrameWidth, Constants::cFrameHeight );
}


BEGIN_EVENT_TABLE( GraphPane, wxPanel )

EVT_PAINT( GraphPane::OnPaint )
EVT_PAINT( GraphPane::OnEraseBackground )
EVT_LEFT_DOWN( GraphPane::OnMouse )

END_EVENT_TABLE( )
